# RPi GelDoc 


## Description
This project is to develop a low-cost geldoc system tailored to imaging gels stained with thiazole orange. The goal is to use top-down illumination rather than transillumination and an RPi camera for imaging combined with filters to cut out background light. As the source of light and the camera are small, the filters are also small which cuts down the cost.

## Documentation

 - [Filter Choices](./docs/filter-choices)


## Contributing
We welcome contributions! Add an issue or message cambridge@openbioeconomy.org to volunteer.

## Authors and acknowledgment
 - Jenny Molloy

## License
CERN OHL-P

## Support and Further Information
Email cambridge@openbioeconomy.org

