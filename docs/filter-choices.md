# Thiazole Orange Spectrum

Link to [interactive spectrum](https://www.aatbio.com/fluorescence-excitation-emission-spectrum-graph-viewer/Thiazole_Orange)

![TO Spectrum](../images/TO-spectrum.png)

Aim is to cut out background noise by using a shortpass filter on the excitation light source and a longpass filter on the camera sensor:
 -  a 500 nm shortpass filter (only allowing excitation wavelengths of up to approx 500 nm). Selected Comar Instruments [530 GY 25](https://www.comaroptics.com/pdf/530%20GY%2025.pdf).
 -  a 530 nm longpass filter (only allowing imaging of emmission wavelengths of above approx 530 nm). Selected Comar Instruments [500 IK 25](https://www.comaroptics.com/pdf/500%20IK%2025.pdf).
